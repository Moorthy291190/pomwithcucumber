package pages;

import cucumber.api.java.en.And;

import libraries.Annotations;

public class EditLeadPage extends Annotations{

	@And("Type Companyname as (.*)")
	public EditLeadPage typeCompanyName(String cName)
	{
		driver.findElementById("updateLeadForm_companyName").clear();
		driver.findElementById("updateLeadForm_companyName").sendKeys(cName);
		
		
		return this;
	}
	@And("Click Update Button")
	public ViewLeadPage clickUpdateButton() throws InterruptedException
	{
		driver.findElementByXPath("(//input[@name='submitButton'])[1]").click();
		Thread.sleep(2000);
		return new ViewLeadPage();
	}
	
}
