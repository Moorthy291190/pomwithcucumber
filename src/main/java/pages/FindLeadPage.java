package pages;

import cucumber.api.java.en.Given;
import libraries.Annotations;

public class FindLeadPage extends Annotations {

	@Given("Type FirstName as (.*)")
	public FindLeadPage typeFirstName(String fName)
	{
		driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys(fName);
		return this;
	}
	@Given("Click the FindLead Button")
	public FindLeadPage clickFindLeadButton() throws InterruptedException
	{
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(3000);
		return this;
	}
	@Given ("Click the FindLeadID")
	public ViewLeadPage clickLeadID()
	{
		driver.findElementByXPath("(//a[@class='linktext'])[4]").click();
		return new ViewLeadPage();
	}
	
	
}
