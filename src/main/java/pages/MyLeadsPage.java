package pages;

import cucumber.api.java.en.Given;
import libraries.Annotations;

public class MyLeadsPage extends Annotations {
	
	@Given("Click Create Leads Link")
	public CreateLeadPage clickCreateLead() {
		driver.findElementByLinkText("Create Lead")
		.click();
		return new CreateLeadPage();
		
	}
	@Given("Click the Find Leads Tab")
	public FindLeadPage clickFindLeadTab()
	{
		driver.findElementByLinkText("Find Leads").click();
		return new FindLeadPage();
	}
	@Given("Click the Merge Leads Tab")
	public MergeLeadPage clickMergeLeadTab()
	{
		
		driver.findElementByLinkText("Merge Leads").click();
		return new MergeLeadPage();
		
	}

}
