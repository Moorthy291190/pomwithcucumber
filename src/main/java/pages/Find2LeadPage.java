package pages;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import cucumber.api.java.en.And;
import libraries.Annotations;

public class Find2LeadPage extends Annotations {

	@And("Type First2Name as (.*)")
	public Find2LeadPage TypeFirst2Name(String fname) throws InterruptedException
	{
		driver.findElementByName("firstName").sendKeys(fname);
		Thread.sleep(2000);
		return this;
		}
	@And("Click Find2Lead Button")
	public Find2LeadPage click2FindLeadButton() throws InterruptedException
	{		
		driver.findElementByXPath("(//button[@class='x-btn-text'])[1]").click();
		Thread.sleep(2000);
		return this;
		}
	@And("Click Lead@ID Button")
	public MergeLeadPage Click2LeadID() throws InterruptedException
	{		
		driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
		Thread.sleep(2000);
		Set<String> windows = driver.getWindowHandles();
		List<String>wins=new ArrayList<String>(windows);
		driver.switchTo().window(wins.get(0));
		
		return new MergeLeadPage();
		}

}
