package pages;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import libraries.Annotations;

public class ViewLeadPage extends Annotations {
	
	@Then("Verify First Name (.*)")
	public ViewLeadPage verifyFirstName(String fName) {
		String text = driver.findElementById("viewLead_firstName_sp")
		.getText();
		if(text.equals(fName)) {
			System.out.println("First name matches with input data "+fName);
		}else {
			System.err.println("First name not matches with input data "+fName);
		}
		return this;
	}
	@And ("Click Delete Button")
	public MyLeadsPage clickDeleteButton()
	{
		driver.findElementByLinkText("Delete").click();
		return new MyLeadsPage();
	}
@And("Click Duplicate Button")
public DuplicateLeadPage clickDuplicateButton()
{
	driver.findElementByLinkText("Duplicate Lead").click();
	return new DuplicateLeadPage();}
@And("Click Edit Button")
public EditLeadPage clickEditButton()
{
	driver.findElementByXPath("(//a[@class='subMenuButton'])[3]").click();
	return new EditLeadPage();
	}
	
	
	
	
	
	
}
