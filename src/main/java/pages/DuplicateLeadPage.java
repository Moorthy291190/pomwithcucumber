package pages;

import cucumber.api.java.en.And;
import libraries.Annotations;

public class DuplicateLeadPage extends Annotations {

	@And("Type CompanyName as (.*)")
	public DuplicateLeadPage typeCompanyName(String cName)
	{
		driver.findElementById("createLeadForm_companyName").clear();
		driver.findElementById("createLeadForm_companyName").sendKeys(cName);
		return this;
	}
	@And ("Click CreateLeadButton")
public ViewLeadPage clickCreateLeadButton()
{
		driver.findElementByName("submitButton").click();
		return new ViewLeadPage();}
}
