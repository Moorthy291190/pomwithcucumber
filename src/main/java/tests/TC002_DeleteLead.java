package tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import libraries.Annotations;
import pages.MyHomePage;

public class TC002_DeleteLead extends Annotations{

	@BeforeClass
	public void setData() {
		excelFileName = "TC002";
	}
	
	@Test(dataProvider="fetchData")
	public void deleteLeadTest(String fName) throws InterruptedException
	{
		new MyHomePage()
		.clickLeadsTab()
		.clickFindLeadTab()
		.typeFirstName(fName)
		.clickFindLeadButton()
		.clickLeadID()
		.clickDeleteButton();
		
	}
}
