package tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import libraries.Annotations;
import pages.MyHomePage;

public class TC005_MergeLead extends Annotations {

	@BeforeClass
	public void setdata()
	{
		excelFileName="TC005";
	}
	
	@Test(dataProvider="fetchData")
	public void TestMergeLead(String fname,String f2name) throws InterruptedException
	{
		new MyHomePage()
		.clickLeadsTab()
		.clickMergeLeadTab()
		.clickFromLeadIcon()
		.TypeFirst2Name(fname)
		.click2FindLeadButton()
		.Click2LeadID()
		.clickToLeadIcon()
		.TypeFirst2Name(f2name)
		.click2FindLeadButton()
		.Click2LeadID()
		.clickMergeButton();
	}
	
}
