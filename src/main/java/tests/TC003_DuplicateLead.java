 package tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import libraries.Annotations;
import pages.MyHomePage;

public class TC003_DuplicateLead extends Annotations
{
	@BeforeClass
public void setdata()
{
	excelFileName="TC003";
	}
	@Test(dataProvider="fetchData")
public void TestDuplicateLead(String fName,String cName) throws InterruptedException
{
 new MyHomePage()
.clickLeadsTab()
.clickFindLeadTab()
.typeFirstName(fName)
.clickFindLeadButton()
.clickLeadID()
.clickDuplicateButton()
.typeCompanyName(cName)
.clickCreateLeadButton();
}

}
