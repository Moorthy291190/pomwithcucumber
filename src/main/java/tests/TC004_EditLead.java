package tests;


import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import libraries.Annotations;
import pages.MyHomePage;

public class TC004_EditLead extends Annotations {

	@BeforeClass
	public void setdata()
	{
		excelFileName="TC004";
	}
	
	@Test(dataProvider="fetchData")
	public void TestEditLead(String fName,String cName) throws InterruptedException
	{
		new MyHomePage()
		.clickLeadsTab()
		.clickFindLeadTab()
		.typeFirstName(fName)
		.clickFindLeadButton()
		.clickLeadID()
		.clickEditButton() 
		.typeCompanyName(cName)
		.clickUpdateButton();
		
	}
}
